\documentclass{book}
\usepackage[dvips]{graphicx}
\usepackage{amsmath,amsfonts,amssymb}

\setlength{\textwidth}{160mm}
\setlength{\oddsidemargin}{0mm}
\setlength{\evensidemargin}{0mm}
\setlength{\textheight}{240mm}
\setlength{\topmargin}{0mm}
\setlength{\parskip}{0.1\baselineskip}
\setlength{\footnotesep}{4.5mm}

\def\thesection{\arabic{section}}
%\bibliographystyle{GABtoOUP}% file c:\TeXLocalFiles\bibtex\bst\Gab\GABtoOUP.bst

\begin{document}
\thispagestyle{empty}
\begin{center}
{\Large\bfseries Disc in a quadrupole field}
\end{center}


\section{Introduction}\label{s1}
A thin conducting disc is placed in an electrostatic field whose potential is
\begin{equation}\label{e1}
\varphi_{\text{applied}}=A(x^2+y^2-2z^2).
\end{equation}
This is, of course, a solution of Laplace's equation. The disc has its centre at the origin of coordinates, and is free to rotate. Charges are induced in the disc by the field, and those charges experience forces from the field which in turn exert a torque on the disc. Our task is to find the induced charges and the torque.
\begin{figure}[h]
\centering\includegraphics{QuadrupoleFieldLines.eps}\caption{\label{f1}Field lines for the quadrupole potential $\varphi_{\text{applied}}=A(x^2+y^2-2z^2)$, shown in the $xz$ plane. The slanting line is the equipotential $\varphi=0$ and, being an equipotential, is perpendicular to all field lines. A conducting rod, lying along this line, has no induced charge movements. The line has $z=x/\sqrt{2}$; equivalently $\varTheta=\varTheta_1$ where $\mathrm{P}_2(\cos\varTheta_1)=\frac{1}{2}(3\cos^2\varTheta_1-1)=0$.}
\end{figure}


\subsection{A conducting rod}\label{s1.1}
Before embarking on a detailed calculation for a disc, we may get a feel for things by considering a simpler case, that of a conducting rod that lies entirely in the $xz$ plane. The field lines in the $xz$ plane are drawn in Fig.~\ref{f1}. A possible orientation for the rod is indicated by the sloping line which is the equipotential $\varphi=0$; it is perpendicular to all field lines. So, for this orientation, the rod has no charge movements; there is no induced quadrupole moment, and the rod experiences no torque.

If the rod is tilted to a smaller $\varTheta$, the field lines have components along the rod, and tend to move positive charge from centre to ends: a positive quadrupole moment. It is easy to see that the charges experience forces that tend further to reduce $\varTheta$: the rod ends up lying along the $z$-axis. Conversely, if $\varTheta$ is increased from $\varTheta_1$, the field projected onto the rod tends to move positive charge from ends to centre, resulting in a torque that tends further to increase $\varTheta$ until the rod lies in the $xy$ plane.

The rod orientation drawn in Fig.~\ref{f1} is therefore an unstable equilibrium, a potential-energy maximum.

The potential $\varphi$ gives a field that is twice as strong in the $z$-direction as in the $x$-direction. Therefore, comparing $\varTheta=0$ and $\varTheta=\pi/2$, the induced charges should be twice as great for $\varTheta=0$, and, experiencing a double field, should have a potential-energy minimum that is $4\times$ deeper.

Summarizing: a conducting rod has a stable potential-energy minimum at $\varTheta=0$; an unstable potential-energy maximum at $\varTheta_1=\arccos(1/\sqrt{3})$; a metastable (shallower) potential-energy minimum at $\varTheta=\pi/2$.

The applied field has axial symmetry about $Oz$. So everything said about the rod remains true if the rod is rotated about $Oz$: so far as this rotation is concerned, the equilibria are all neutral.

A disc differs from a rod in having conductor at locations $y\ne0$ away from the $xz$ plane. We must expect that the curved outline of the disc will affect the induced charged distribution, though the qualitative insights from the rod should carry over to the case of the disc.


\section{Details of Figure \ref{f1}}\label{s2}
Given the potential $\varphi$ of eqn \eqref{e1}, lines of constant potential $\varphi$ have (for $y=0$)
\begin{equation*}
\frac{\mathrm{d}z}{\mathrm{d}x}=\frac{x}{2z}.
\end{equation*}
The field lines are the orthogonal trajectories of these equipotentials, so they have gradient
\begin{equation*}
\frac{\mathrm{d}z}{\mathrm{d}x}=\frac{-1}{x/2z}=\frac{-2z}{x},
\end{equation*}
which integrates to $zx^2=\text{constant}$. The curves plotted are the result of giving the constant the values 1, 5, 20, 100 (and the negatives of these).


\section{The applied potential}\label{s3}
\begin{figure}[h]
\centering\includegraphics{DiscAxes.eps}\caption{\label{f2} The conducting disc lies at an angle to the axes that define the electrostatic field according to eqn \eqref{e1}, such that its normal lies at angle $\vartheta$ to the $z$-axis. Axes $(X,Y,Z)$ are ``local'' to the disc.}
\end{figure}
A conducting disc is placed in the electrostatic potential of eqn \eqref{e1}, centred on the origin, and is tilted so that its normal $OZ$ makes angle $\vartheta$ ($=\varTheta-\pi/2$) to the $z$-axis. We define $XYZ$ axes ``local'' to the disc such that $Y=y$, which makes $XZ$ lie in the $xz$ plane. Old and new axes are shown in Fig.~\ref{f2}.

The relation between old and new coordinates is
\begin{equation*}
x=X\cos\vartheta+Z\sin\vartheta;\qquad y=Y;\qquad z=-X\sin\vartheta+Z\cos\vartheta.
\end{equation*}
Then the potential $\varphi$, expressed in the new coordinates, is given by
\begin{align}
\left(\frac{\varphi_{\text{applied}}}{A}\right)_{\!\text{everywhere}}&=
(X\cos\vartheta+Z\sin\vartheta)^2+Y^2-2(-X\sin\vartheta+Z\cos\vartheta)^2\notag\\
&=X^2(1-3\sin^2\vartheta)+Y^2+6XZ\sin\vartheta\cos\vartheta+
Z^2(1-3\cos^2\vartheta).\label{e2}
\end{align}
Boundary conditions will be applied in the plane of the disc, so at $Z=0$ where
\begin{equation}\label{e3}
\left(\frac{\varphi_{\text{applied}}}{A}\right)_{\!Z=0}=
X^2(1-3\sin^2\vartheta)+Y^2=(X^2+Y^2)-3X^2\sin^2\vartheta.
\end{equation}
This is the potential that would exist in the plane of the disc if the disc were absent. Charges will be induced in the disc, whose potential $\varphi_{\text{induced}}$ will cancel the applied potential to give a total that is constant over the surface of the disc.


\section{Oblate spheroidal coordinates}\label{s4}
The coordinate system that is ideal for solving Laplace's equation (and incidentally the wave equation) in the vicinity of a disc is the oblate spheroidal system. Coordinates $\mu,\theta,\phi$ are related to $X,Y,Z$ by
\begin{equation}\label{e4}
X=a\cosh\mu\sin\theta\cos\phi;\qquad Y=a\cosh\mu\sin\theta\sin\phi;\qquad
Z=a\sinh\mu\cos\theta.
\end{equation}
Figure \ref{f3} shows a representative spheroid and a representative hyperboloid. The spheroid is a surface of constant $\mu\geqslant0$. The hyperboloid has the shape of a cooling tower, and is a surface of constant~$\theta$. But there is a discontinuity in the coordinate system where the hyperboloid crosses the conducting disc: $\theta$~changes discontinuously to $\pi-\theta$. We define $\theta$ so that it lies between $(0,\pi/2)$ when $Z>0$ and between $(\pi,\pi/2)$ when $Z<0$. More usefully, we can say that $\cos\theta$ takes the sign of $Z$ while $\sinh\mu$ is positive (or zero) always.%
\footnote{The coordinate system necessarily has a discontinuity in part of the $XY$ plane. We have made that discontinuity coincide with the disc, because there the field lines normal to the disc reverse direction: a discontinuity in the coordinate system matches a discontinuity in the physics. Had the physical problem involved a hole in a conducting plane, we would have made the discontinuity lie at $X^2+Y^2>a^2$. Equations \eqref{e3} would still hold, but $\mu$ would take the sign of $Z$ and be discontinuous in sign where each spheroid crosses the $XY$ plane, while $\theta$ would be continuous and always positive.

Notice that the coordinate system naturally allows the physics to be different on the upper and lower faces of the disc, at $Z=0+$ and $Z=0-$, because $\theta$ takes different values.}
\begin{figure}
\centering\includegraphics{OblateCoords.eps}\caption{\label{f3}Oblate spheroidal coordinates, as defined in \eqref{e4}. The disc is shown in section as the heavy line. In this section, all ellipses and all hyperbolae have common foci at $X=\pm a$. Each spheroid is a surface $\mu=\text{constant}$ with $\mu\geqslant0$. Each hyperboloid of revolution is a surface of constant $\theta$, but it changes its $\theta$ to $\pi-\theta$ where it passes through the conductor. The plane of the disc is defined by $\mu=0$.}
\end{figure}

The surfaces of constant $\mu$ are oblate (squashed-orange) spheroids. The surface of the disc is a limitingly thin spheroid having $\mu=0$. Increase of $\mu$ gives a larger spheroid. The surfaces of constant $\theta$ are hyperboloids of revolution, broader for larger $\sin\theta$. If we have difficulty visualizing how these coordinates specify locations, perhaps it's easiest to think of the larger distances from the origin, where $\cosh\mu\approx\sinh\mu$, because then the coordinates approximate to spherical polars with $r,\theta,\phi$ replaced by $a\sinh\mu,\theta,\phi$.

We shall need to express the potential in terms of the spheroidal coordinates. In the plane of the disc $Z=0$ we have $\mu=0$, $\sinh\mu=0$, $\cosh\mu=1$. Also, $\phi$ is the usual azimuth coordinate in the $XY$ plane. Reversing the sign from \eqref{e3} gives the induced potential:
\begin{equation*}
\bigg(\frac{\varphi_{\text{induced}}}{Aa^2}\bigg)_{\!Z=0}=
\sin^2\theta\big(3\sin^2\vartheta\cos^2\phi-1\big).
\end{equation*}
As a consistency check, we note that, in the plane of Fig.~\ref{f1}, where $\phi=0$, this is zero at $\cos^2\varTheta=\sin^2\vartheta=\frac{1}{3}$, the zero-induced-charge condition inferred from Fig.~\ref{f1}.

For later use, we rearrange the expression for $\varphi_{\text{induced}}$ so as to display the presence of spherical-harmonic functions in $\theta,\phi$ (the reason for which will appear later):
\begin{align*}
\bigg(\frac{\varphi_{\text{induced}}}{Aa^2}\bigg)_{\!Z=0}&=
\sin^2\theta\bigg\{3\sin^2\vartheta\bigg(\frac{1+\cos(2\phi)}{2}\bigg)-1\bigg\}\notag\\
&=
\bigg(\frac{3}{2}\sin^2\vartheta-1\bigg)\sin^2\theta+\bigg(\frac{1}{2}\sin^2\vartheta\bigg)
3\sin^2\theta\cos(2\phi).
\end{align*}
A further rearrangement gives
\begin{align}
\bigg(\frac{\varphi_{\text{induced}}}{Aa^2}\bigg)_{\!Z=0}&=
\bigg(\frac{3}{2}\sin^2\vartheta-1\bigg)
\bigg(\frac{2}{3}-\frac{2}{3}\,\frac{(3\cos^2\theta-1)}{2}\bigg)+
\bigg(\frac{1}{2}\sin^2\vartheta\bigg)3\sin^2\theta\cos(2\phi)\notag\\
&=\bigg(\frac{3}{2}\sin^2\vartheta-1\bigg)
\bigg(\frac{2}{3}\,\mathrm{P}_0(\cos\theta)-\frac{2}{3}\,\mathrm{P}_2(\cos\theta)\bigg)
+\bigg(\frac{1}{2}\sin^2\vartheta\bigg)\mathrm{P}_2^2(\cos\theta)\cos(2\phi)\notag\\
&=\bigg(\sin^2\vartheta-\frac{2}{3}\bigg)
\Big\{1-\mathrm{P}_2(\cos\theta)\Big\}+
\bigg(\frac{1}{2}\sin^2\vartheta\bigg)\mathrm{P}_2^2(\cos\theta)\cos(2\phi).\label{e5}
\end{align}
(The functions of $\vartheta$ can also be made to display $\mathrm{P}_2(\cos\vartheta)$ and $\mathrm{P}_2^2(\cos\vartheta)$, but such a rearrangement does not help us.)

Our task now is to solve the potential problem that gives $\varphi_{\text{induced}}$ over all space, hence to find the charge distribution over the disc, and thence to find the torque experienced by the disc as these charges interact with the applied field.


\section{Laplace's equation}\label{s5}
In the $\mu,\theta,\phi$ system, Laplace's equation is given by the rather ugly expression
\begin{equation}\label{e6}
0=a^2\nabla^2\varphi=\frac{1}{(\cosh^2\mu-\sin^2\theta)}\left(
\frac{\partial^2\varphi}{\partial\mu^2}+\tanh\mu\,\frac{\partial\varphi}{\partial\mu}+
\frac{\partial^2\varphi}{\partial\theta^2}+
\cot\theta\,\frac{\partial\varphi}{\partial\theta}\right)+
\frac{1}{\cosh^2\mu\sin^2\theta}\,\frac{\partial^2\varphi}{\partial\phi^2}.
\end{equation}
The spheroidal coordinates that lead to this equation are said to be ``separable'' because a solution can be found that factorizes into a function of $\mu$, a function of $\theta$, and a function of $\phi$. A first separation of variables yields quickly
\begin{equation*}
\varphi=M(\mu)S(\theta)\varPhi(\phi) \quad \text{in which}\quad
\varPhi=\mathrm{e}^{\pm\mathrm{i}m\phi}\quad\text{with $m$ an integer}.
\end{equation*}
A further separation then gives
\begin{equation*}
\frac{\mathrm{d}^2M}{\mathrm{d}\mu^2}+\tanh\mu\,\frac{\mathrm{d}M}{\mathrm{d}\mu}+
\frac{m^2}{\cosh^2\mu}\,M-\lambda M=0;\qquad
\frac{\mathrm{d}^2S}{\mathrm{d}\theta^2}+
\cot\theta\,\frac{\mathrm{d}S}{\mathrm{d}\theta}-
\frac{m^2}{\sin^2\theta}\,S+\lambda S=0.
\end{equation*}
Here $\lambda$ is an eigenvalue still to be determined.

Now that the separation of variables has been performed, we can change the variables from $\mu,\theta$ to the more convenient $u=\sinh\mu$, $s=\cos\theta$. These changes result in:
\begin{align}
(1+u^2)\,\frac{\mathrm{d}^2M}{\mathrm{d}u^2}+2u\,\frac{\mathrm{d}M}{\mathrm{d}u}+
\frac{m^2}{1+u^2}\,M-\lambda M&=0;\label{e7}\\
(1-s^2)\,\frac{\mathrm{d}^2S}{\mathrm{d}s^2}-2s\,\frac{\mathrm{d}S}{\mathrm{d}s}-
\frac{m^2}{1-s^2}\,S+\lambda S&=0.\label{e8}
\end{align}

Equation \eqref{e8} is the associated Legendre equation---perhaps not surprising because only the ``radial'' functions $M$ in $X,Y,Z$ differ from the spherical-polar case. If $S$ is not to blow up at $s=\pm1$ we must give $\lambda$ the eigenvalue $l(l+1)$ with $l$ an integer or zero. We now have
\begin{equation*}
S=\mathrm{P}_l^m(\cos\theta),\qquad S\varPhi=\mathrm{P}_l^m(\cos\theta)\,\mathrm{e}^{\pm\mathrm{i}m\phi}.
\end{equation*}
Now the boundary condition \eqref{e5} contains $\cos(0\phi)$ and $\cos(2\phi)$, so it makes sense here to choose $\varPhi$ to contain a cosine rather than a complex exponential, so we write
\begin{equation}\label{e9}
S=\mathrm{P}_l^m(\cos\theta),\qquad
S\varPhi=\mathrm{P}_l^m(\cos\theta)\cos(m\phi).
\end{equation}
We see from \eqref{e5} that the only values of $m$ that interest us will be $m=0$ and $m=2$.

The differential equation for $M$ becomes
\begin{equation}\label{e10}
(1+u^2)\,\frac{\mathrm{d}^2M}{\mathrm{d}u^2}+2u\,\frac{\mathrm{d}M}{\mathrm{d}u}+
\frac{m^2}{1+u^2}\,M-l(l+1)M=0.
\end{equation}
We shall need to find the functions $M_l^m$ that conform to the boundary condition \eqref{e5}, and also (less obviously) one that represents the physics of a charged disc in a field-free region.


\section[The radial functions $M_{\text{\lowercase{$l$}}}^{\text{\lowercase{$m$}}}$]{The radial functions $\boldsymbol{M}_l^m$}\label{s6}
%% Extraordinary measures to force _l^m not to be made upper case in the running page
%%  head
Let us think first about a disc that is charged and has no externally imposed field. At large distances, the potential will be that of a point charge at the origin, so independent of $\theta$ and $\phi$. This means it has $l=0$ and $m=0$. Putting $l=m=0$ in \eqref{e10} results in
\begin{equation*}
\frac{\mathrm{d}}{\mathrm{d}u}
\left((1+u^2)\,\frac{\mathrm{d}M_0}{\mathrm{d}u}\right)=0;\qquad
(1+u^2)\,\frac{\mathrm{d}M_0}{\mathrm{d}u}=\text{constant};\qquad
M_0\propto(\arctan u+\text{constant})\propto\pi/2-\arctan u.
\end{equation*}
Here the added $\pi/2$ is necessary to make sure that $M_0\rightarrow0$ as $\mu\rightarrow\infty$. When substitutions are made back to the $XYZ$ coordinates, this agrees with the potential obtained by Landau, Lifshitz and Pitaevski\u{\i} (1993), \S\,4, problem~1. At large distances, $M_0\rightarrow1/u\propto1/r$, which is appropriate to the potential of a point charge at the origin.

To solve eqn \eqref{e10} in general, we write $\rho=-\mathrm{i}u$ and re-express the equation with $\rho$ as the independent variable:
\begin{equation*}
\setlength{\belowdisplayshortskip}{\belowdisplayskip}
(1-\rho^2)\,\frac{\mathrm{d}^2M}{\mathrm{d}\rho^2}-
2\rho\,\frac{\mathrm{d}M}{\mathrm{d}\rho}-\frac{m^2}{1-\rho^2}\,M+l(l+1)M=0.
\end{equation*}
This is the associated Legendre equation again, so it has solutions $\mathrm{P}_l^m(\rho)$ and $\mathrm{Q}_l^m(\rho)$. This time we cannot reject the $Q$ solutions, so both kinds of solution must be retained.

It will suffice to catalogue a few of the low-$l$ solutions for $M$.

We already have one solution for the case $l=0$.
\begin{equation*}
\mathrm{P}_0(\rho)=1,\qquad
\mathrm{Q}_0(\rho)=\frac{1}{2}\,\ln\left(\frac{1+\rho}{1-\rho}\right)=
\frac{1}{2}\,\ln\left(\frac{1-\mathrm{i}u}{1+\mathrm{i}u}\right)=
-\mathrm{i}\,\arctan u.
\end{equation*}
A linear combination of these two functions gives the solution already found:
\begin{equation}\label{e11}
M_0\equiv\frac{\pi}{2}\,\mathrm{P}_0-\mathrm{i}\mathrm{Q}_0=\frac{\pi}{2}-\arctan u.
\end{equation}

For $l=2$, $m=0$,
\begin{equation*}
\setlength{\belowdisplayshortskip}{\belowdisplayskip}
\mathrm{P}_2(\rho)=\frac{1}{2}(3\rho^2-1)=\frac{1}{2}(-1-3u^2),
\end{equation*}
and for the other solution (Abramowitz and Stegun, 1965, relation 8.4.6):
\begin{align*}
\mathrm{Q}_2(\rho)&=
\left(\frac{3\rho^2-1}{4}\right)\ln\left(\frac{1+\rho}{1-\rho}\right)-
\frac{3\rho}{2}=
\left(\frac{-1-3u^2}{4}\right)\ln\left(\frac{1-\mathrm{i}u}{1+\mathrm{i}u}\right)+
\frac{3\mathrm{i}u}{2}\\
&=\mathrm{i}\left\{\left(\frac{1+3u^2}{2}\right)\arctan u+\frac{3u}{2}\right\}.
\end{align*}
We need a physically acceptable total, meaning one that goes to zero for large $u$, so we form the linear combination:
\begin{equation}\label{e12}
M_2\equiv\frac{-\pi}{2}\,\mathrm{P}_2+\mathrm{i}\mathrm{Q}_2=
\frac{(1+3u^2)}{2}\left(\frac{\pi}{2}-\arctan u\right)-\frac{3u}{2}.
\end{equation}

We also need $\mathrm{P}_2^2(\rho)$ and $\mathrm{Q}_2^2(\rho)$.
{\setlength{\belowdisplayskip}{\belowdisplayshortskip}
\begin{align}
\mathrm{P}_2^2(\rho)&=3(1-\rho^2)=3(1+u^2);\notag\\
\mathrm{Q}_2^2(\rho)&=\frac{3}{2}(1-\rho^2)\ln\left(\frac{1+\rho}{1-\rho}\right)+
\frac{\rho(5-3\rho^2)}{(1-\rho^2)}\notag\\
&=-\mathrm{i}\left(3(1+u^2)\arctan u+\frac{u(5+3u^2)}{1+u^2}\right).\notag\\
M_2^2(u)&\equiv\frac{\pi}{2}\,\mathrm{P}_2^2(u)-\mathrm{i}\,\mathrm{Q}_2^2(u)\notag\\
&=3(1+u^2)\left(\frac{\pi}{2}-\arctan u\right)-\frac{u(5+3u^2)}{1+u^2}.\label{e13}
\end{align}}%

Gathering results,
{\setlength{\abovedisplayskip}{\abovedisplayshortskip}
\begin{align}
M_0(u)&=\frac{\pi}{2}-\arctan u,\tag{\ref{e11}}\\
M_2(u)&=\frac{(1+3u^2)}{2}\left(\frac{\pi}{2}-\arctan u\right)-
\frac{3u}{2},\tag{\ref{e12}}\\
M_2^2(u)&=3(1+u^2)\left(\frac{\pi}{2}-\arctan u\right)-
\frac{u(5+3u^2)}{1+u^2}.\tag{\ref{e13}}
\end{align}}%


\section{The solution of the potential problem}\label{s7}
The most general solution of the Laplace equation in oblate spheroidal coordinates must be a sum of eigenfunctions:
\begin{equation}\label{e14}
\setlength{\belowdisplayshortskip}{\belowdisplayskip}
\frac{\varphi_{\text{induced}}}{Aa^2}=\sum_l\sum_m C_{lm}\,M_l^m(u)\,\mathrm{P}_l^m(\cos\theta)\cos(m\phi),
\end{equation}
where the $C_{lm}$ are constant coefficients to be found. For the present problem, we have to make this sum add up to the function of eqn \eqref{e5}, in the plane of the disc where $u=\sinh\mu=0$. The only functions of $\theta,\phi$ in \eqref{e5} have $l=0$ and $l=2$ with $m=0,2$, so the sum reduces to three terms only:
\begin{align*}
\frac{\varphi_{\text{induced}}}{Aa^2}&=
C_{00}\,M_0(u)\,\mathrm{P}_0(\cos\theta)\cos(0\phi)+
C_{20}\,M_2(u)\,\mathrm{P}_2(\cos\theta)\cos(0\phi)+
C_{22}\,M_2^2(u)\,\mathrm{P}_2^2(\cos\theta)\cos(2\phi)\\
&=C_{00}\,M_0(u)+C_{20}\,M_2(u)\,\mathrm{P}_2(\cos\theta)+
C_{22}\,M_2^2(u)\,\mathrm{P}_2^2(\cos\theta)\cos(2\phi).
\end{align*}

To find the coefficients, we set $u=0$ and equate to \eqref{e5}.
\begin{align*}
&C_{00}\,M_0(0)+C_{20}\,M_2(0)\,\mathrm{P}_2(\cos\theta)+
C_{22}\,M_2^2(0)\,\mathrm{P}_2^2(\cos\theta)\cos(2\phi)\\
&\quad=
\bigg(\sin^2\vartheta-\frac{2}{3}\bigg)
\Big\{1-\mathrm{P}_2(\cos\theta)\Big\}+
\bigg(\frac{1}{2}\sin^2\vartheta\bigg)\mathrm{P}_2^2(\cos\theta)\cos(2\phi).
\end{align*}
Remember that the radial coordinate in the disc is $a\sin\theta$, so functions of $\theta$ and $\phi$ must match for all $\theta,\phi$. We equate coefficients:
\begin{alignat*}{2}
C_{00}\,M_0(0)&=\left(\sin^2\vartheta-\frac{2}{3}\right),&\qquad
C_{00}&=\frac{2}{\pi}\left(\sin^2\vartheta-\frac{2}{3}\right);\\
C_{20}\,M_2(0)&=-\left(\sin^2\vartheta-\frac{2}{3}\right),&\qquad
C_{20}&=\frac{-4}{\pi}\left(\sin^2\vartheta-\frac{2}{3}\right);\\
C_{22}\,M_2^2(0)&=\frac{1}{2}\,\sin^2\vartheta,&\qquad
C_{22}&=\frac{1}{3\pi}\,\sin^2\vartheta.
\end{alignat*}

Gathering together the coefficients just found, we have our eigenfunction solution for the induced potential surrounding the disc:
\begin{align}
\frac{\varphi_{\text{induced}}}{Aa^2}&=
C_{00}\,M_0(u)+C_{20}\,M_2(u)\,\mathrm{P}_2(\cos\theta)+
C_{22}\,M_2^2(u)\,\mathrm{P}_2^2(\cos\theta)\cos(2\phi)\notag\\
&=\frac{2}{\pi}\left(\sin^2\vartheta-\frac{2}{3}\right)
\bigg\{\frac{\pi}{2}-\arctan u\bigg\}\notag\\
&\quad-\frac{4}{\pi}\left(\sin^2\vartheta-\frac{2}{3}\right)
\left\{\frac{(1+3u^2)}{2}\left(\frac{\pi}{2}-\arctan u\right)-\frac{3u}{2}\right\}
\mathrm{P}_2(\cos\theta)
\notag\\
&\quad+
\frac{1}{3\pi}\,\sin^2\vartheta\left\{
3(1+u^2)\left(\frac{\pi}{2}-\arctan u\right)-\frac{u(5+3u^2)}{1+u^2}\right\}
\mathrm{P}_2^2(\cos\theta)\cos(2\phi).\label{e15}
\end{align}
This expression holds for all locations in space, not just at the disc's surface.

We can understand \eqref{e15} by looking at how the potential behaves at large distances from the disc. At large distances, the spheroids in Fig.~\ref{f3} approximate to spheres of radius $a\cosh\mu\approx a\sinh\mu=au$, so $u$ is a scaled radius in this limit. Also, $\theta$ becomes the usual polar angle, so $(au,\theta,\phi)$ become spherical polar coordinates. At these large distances, $M_0\rightarrow1/u$, $M_2\rightarrow2/15u^3$ and $M_2^2\rightarrow8/5u^3$. Thus in \eqref{e15}, the first term is the potential due to a charge at the origin. Similarly, the second and third terms have the shapes $r^{-3}\,\mathrm{P}_2^m(\cos\theta)\cos(m\phi)$
which are the signature of the potential set up by a quadrupole moment at the origin. Finally, we note that the absence of $r^{-2}\,\mathrm{P}_1^m(\cos\theta)\cos(m\phi)$ signals the absence of an induced electric dipole moment.

Now the disc must remain uncharged when put into the quadrupole field. But \eqref{e15} gives a potential that includes a contribution from a non-zero total charge. By superposition, we can add a cancelling charge without violating any of the conditions of the problem, so we do that and henceforth omit the first term in \eqref{e15}.

We would expect that charges induced on the disc would give a potential that is symmetrical about the plane of the disc: even with $Z$, even with $\cos\theta$. This symmetry can be instantly confirmed from \eqref{e15}.

The corrected replacement for \eqref{e15} now reads
\begin{align}
\frac{\varphi_{\text{induced}}}{Aa^2}&=C_{20}\,M_2(u)\,\mathrm{P}_2(\cos\theta)+
C_{22}\,M_2^2(u)\,\mathrm{P}_2^2(\cos\theta)\cos(2\phi)\notag\\
&=-\frac{4}{\pi}\left(\sin^2\vartheta-\frac{2}{3}\right)
\left\{\frac{(1+3u^2)}{2}\left(\frac{\pi}{2}-\arctan u\right)-\frac{3u}{2}\right\}
\mathrm{P}_2(\cos\theta)
\notag\\
&\quad+
\frac{1}{3\pi}\,\sin^2\vartheta\left\{
3(1+u^2)\left(\frac{\pi}{2}-\arctan u\right)-\frac{u(5+3u^2)}{1+u^2}\right\}
\mathrm{P}_2^2(\cos\theta)\cos(2\phi).\label{e16}
\end{align}


\section{The disc's surface charge density}\label{s8}
We have divided the field into the applied field $-\nabla\varphi_{\text{applied}}$ and the induced field $-\nabla\varphi_{\text{induced}}$. Field lines of the applied field pass through the location of the disc unaffected (by definition of ``applied''): what enters through one face of the disc leaves through the other, leaving behind no difference that could contribute to a surface charge. Therefore the charge induced on the surfaces of the disc is related wholly to the gradient of the \emph{induced} potential.

The induced potential is an even function of $\cos\theta$, therefore of $Z$, so the field $E_Z$ is an odd function of $Z$, which in turn makes the surface charge density an even function: the charge density is the same on both faces of the disc. For convenience we shall calculate the charge density on the $Z>0$ face of the disc, then doubling to get the total charge density~$\sigma$.
\begin{equation*}
\setlength{\belowdisplayshortskip}{\belowdisplayskip}
\sigma=2\bigg(D_{\text{normal}}\bigg)_{\!Z=0+}=
-2\epsilon_0\left(\frac{\partial\varphi_{\text{induced}}}{\partial Z}\right)_{\!Z=0+}.
\end{equation*}

In Fig.~\ref{f3}, the surfaces of constant $\theta,\phi$ are normal to the disc, so when $Z$ is increased from zero, $\mu$ changes but $\theta$ and $\phi$ do not (in first order). From \eqref{e4},
\begin{equation*}
Z=a\sinh\mu\cos\theta=au\cos\theta;\qquad \delta Z=a\cos\theta(\delta u);\qquad
\frac{\partial\varphi}{\partial Z}=
\frac{1}{a\cos\theta}\,\frac{\partial\varphi}{\partial u}.
\end{equation*}
To evaluate this, we need to differentiate the functions of $u$ in \eqref{e16}.
\begin{align*}
M_2(u)&=\frac{(1+3u^2)}{2}\left(\frac{\pi}{2}-\arctan u\right)-\frac{3u}{2};\\
\frac{\mathrm{d}M_2(u)}{\mathrm{d}u}&=3u\left(\frac{\pi}{2}-\arctan u\right)-
\frac{1+3u^2}{2}\,\frac{1}{1+u^2}-\frac{3}{2},
\end{align*}
which takes the value $-2$ at the surface of the disc. Likewise,
\begin{align*}
M_2^2(u)&=3(1+u^2)\bigg(\frac{\pi}{2}-\arctan u\bigg)-\frac{5u+3u^3}{1+u^2};\\
\frac{\mathrm{d}M_2^2(u)}{\mathrm{d}u}&=
6u\bigg(\frac{\pi}{2}-\arctan u\bigg)-3(1+u^2)\,\frac{1}{1+u^2}-
\frac{5+9u^2}{1+u^2}+\frac{(5u+3u^3)2u}{(1+u^2)^2}
\end{align*}
which takes the value $-8$ at the surface of the disc.

We now have the surface charge density $\sigma$ given by
\begin{align*}
\frac{-\sigma}{\epsilon_0}\,\frac{a\cos\theta}{2Aa^2}&=
\left(\frac{\partial}{\partial u}\frac{\varphi_{\text{induced}}}{Aa^2}\right)_{\!Z=0+}\\
&=
-\frac{4}{\pi}\left(\sin^2\vartheta-\frac{2}{3}\right)(-2)
\mathrm{P}_2(\cos\theta)+
\frac{1}{3\pi}\sin^2\vartheta(-8)\mathrm{P}_2^2(\cos\theta)\cos(2\phi),
\end{align*}
which we can tidy to
\begin{equation}\label{e17}
\sigma=\frac{16}{3\pi}\,\frac{\epsilon_0Aa}{\cos\theta}\bigg\{
\big(2-3\sin^2\vartheta\big)\mathrm{P}_2(\cos\theta)+
\sin^2\vartheta\,\mathrm{P}_2^2(\cos\theta)\cos(2\phi)\bigg\}.
\end{equation}

If we integrate the charge over the surface of the disc, we find a zero total from each of the two terms in \eqref{e16}, consistent with the finding following \eqref{e15}. If we retain the first term in \eqref{e15} we find a non-zero total charge, thereby confirming that that term must be omitted.

From \eqref{e4}, the radial variable in the plane of the disc (where $\cosh\mu=1$) is $\varrho=a\sin\theta$, so that the area element is
$\varrho\,\mathrm{d}\varrho\,\mathrm{d}\phi=
a^2\sin\theta\cos\theta\,\mathrm{d}\theta\,\mathrm{d}\phi$.

The factor $(\cos\theta)^{-1}$ in \eqref{e17} may raise anxiety, since it goes to infinity at the sharp edge of the disc where $\theta=\pi/2$. However, the infinite curvature of the sharp edge means that an infinite charge density, and an infinite electric field, is entirely to be expected. The rise to infinity is slow enough that there is no infinity in the total charge or in the torque; there is a cancelling $\cos\theta$ in the area element.


\section{The torque acting on the disc}\label{s9}
Given that the disc lies in the $XY$ plane, the possible torques acting to rotate it are $\varGamma_X$ (tending to turn it about the $X$-axis), and $\varGamma_Y$ (tending to rotate it about $OY$). The first is related to $\sigma(\boldsymbol{r}\times\boldsymbol{E})_X=\sigma YE_Z$ and the second to $\sigma(\boldsymbol{r}\times\boldsymbol{E})_Y=-\sigma XE_Z$. Now the induced charge $\sigma$ is an even function of $Y$ (an even function of $\phi$), while $(E_{\text{applied}})_Z$ is even with $Y$ also, making $\sigma YE_Z$ odd in~$Y$. Then the contributions to the torque $\varGamma_X$ are odd in $Y$ and sum to zero. We are left with $\varGamma_Y$.

Rewriting the \emph{applied} potential in $XYZ$ coordinates, we have from \eqref{e2}
\begin{equation*}
\setlength{\belowdisplayskip}{\belowdisplayshortskip}
\frac{\varphi_{\text{applied}}}{A}=X^2(1-3\sin^2\vartheta)+Y^2+
6XZ\sin\vartheta\cos\vartheta+Z^2(1-3\cos^2\vartheta).
\end{equation*}
Then
\begin{equation*}
\setlength{\belowdisplayshortskip}{\belowdisplayskip}
-E_Z=\frac{\partial\varphi_{\text{applied}}}{\partial Z}=
6AX\sin\vartheta\cos\vartheta+2AZ(1-3\cos^2\vartheta),
\end{equation*}
which, in the plane of the disc ($Z=0$, $\cosh\mu=1$), reduces to
\begin{equation*}
-E_Z=6AX\sin\vartheta\cos\vartheta=(6A\sin\vartheta\cos\vartheta)a\sin\theta\cos\phi.
\end{equation*}
The turning lever arm is $X=a\sin\theta\cos\phi$, and the area element is $a^2\sin\theta\cos\theta\,\mathrm{d}\theta\,\mathrm{d}\phi$. Therefore the torque $\varGamma_Y$ is given by
\begin{align}
\varGamma_Y&=
\sum(\text{charge/area})\times(\text{area})\times(-\text{field $E_Z$})\times
(\text{lever arm $X$})\notag\\
&=
\int\sigma\times(a^2\sin\theta\cos\theta\,\mathrm{d}\theta\,\mathrm{d}\phi)
\times(6Aa\sin\vartheta\cos\vartheta\,\sin\theta\cos\phi)\times
(a\sin\theta\cos\phi)\notag\\
&=6Aa^4\sin\vartheta\cos\vartheta
\int_0^{\pi/2}\mathrm{d}\theta\int_0^{2\pi}\mathrm{d}\phi\,
\sin^3\theta\cos\theta\cos^2\phi\times\sigma\notag\\
&=\frac{32}{\pi}\epsilon_0A^2a^5\sin\vartheta\cos\vartheta\int_0^{\pi/2}\mathrm{d}\theta
\sin^3\theta\int_0^{2\pi}\mathrm{d}\phi\left(\frac{1+\cos(2\phi)}{2}\right)\notag\\
&\qquad\bigg\{\big(2-3\sin^2\vartheta\big)
\mathrm{P}_2(\cos\theta)+\sin^2\vartheta\,\mathrm{P}_2^2(\cos\theta)\cos(2\phi)\bigg\}
\notag\\
&=\frac{16}{\pi}\epsilon_0A^2a^5\,\sin\vartheta\cos\vartheta
\int_0^{\pi/2}\mathrm{d}\theta\sin^3\theta\int_0^{2\pi}\mathrm{d}\phi
\bigg\{\big(2-3\sin^2\vartheta\big)\mathrm{P}_2(\cos\theta)+
\sin^2\vartheta\,\mathrm{P}_2^2(\cos\theta)\cos^2(2\phi)\bigg\}\notag\\
&=\frac{16}{\pi}\,\epsilon_0A^2a^5\sin\vartheta\cos\vartheta
\int_0^{\pi/2}\mathrm{d}\theta\,\sin^3\theta
\bigg\{\big(2-3\sin^2\vartheta\big)\mathrm{P}_2(\cos\theta)\,2\pi+
\sin^2\vartheta\,\mathrm{P}_2^2(\cos\theta)\,\pi\bigg\}\notag\\
&=16\epsilon_0A^2a^5\,\sin\vartheta\cos\vartheta
\int_0^{\pi/2}\mathrm{d}\theta\sin^3\theta
\bigg\{\big(2-3\sin^2\vartheta\big)(3\cos^2\theta-1)+
\sin^2\vartheta\,3\sin^2\theta\bigg\}\notag\\
&=
16\epsilon_0A^2a^5\sin\vartheta\cos\vartheta\left\{
\frac{-4}{15}\big(2-3\sin^2\vartheta\big)+\frac{8}{5}\sin^2\vartheta
\right\}\notag\\
&=\frac{192}{5}\,\epsilon_0A^2a^5\sin\vartheta\cos\vartheta
\left(\sin^2\vartheta-\frac{2}{9}\right).\label{e18}
\end{align}

Now a positive torque $\varGamma_Y$ tends to increase $\vartheta$, moving the disc's normal from $z$ towards $x$ (Fig.~\ref{f2}). The torque is positive when $\sin^2\vartheta>\frac{2}{9}$, zero when $\sin^2\vartheta=\sin^2\vartheta_1=\frac{2}{9}$; and negative when $\sin^2\vartheta<\frac{2}{9}$. This behaviour is similar to that for the rod described in \S\,\ref{s1.1}, but the unstable equilibrium occurs at a different angle~$\vartheta_1$.


\section{The potential energy}\label{s10}
We can find the potential energy if we integrate $\sigma\varphi_{\text{applied}}$. We must remember that the charge density is induced by the field, and proportional to it, so what we sum is $\frac{1}{2}\sigma\varphi$. From \eqref{e3}, in the plane of the disc,
{\setlength{\belowdisplayskip}{\belowdisplayshortskip}
\begin{align*}
\varphi_{\text{app}}&=A\Big\{(1-3\sin^2\vartheta)X^2+Y^2\Big\}=
Aa^2\Big\{(1-3\sin^2\vartheta)\sin^2\theta\cos^2\phi+\sin^2\theta\sin^2\phi\Big\}\\
&=Aa^2\sin^2\theta\big(1-3\sin^2\vartheta\cos^2\phi\big)\\
&=\frac{Aa^2}{2}\,\sin^2\theta\Big\{(2-3\sin^2\vartheta)-3\sin^2\vartheta\cos(2\phi)\Big\}.
\end{align*}}%
Then
\begin{align}
\text{PE}&=\frac{1}{2}\int(\text{charge density})\times(\text{applied potential})\times(\text{area})\notag\\
&=\frac{1}{2}\int\frac{16}{3\pi}\,\frac{\epsilon_0Aa}{\cos\theta}\Big\{
\big(2-3\sin^2\vartheta\big)\mathrm{P}_2(\cos\theta)+
\sin^2\vartheta\,\mathrm{P}_2^2(\cos\theta)\cos(2\phi)\Big\}\notag\\
&\quad\times \frac{Aa^2}{2}\sin^2\theta\Big\{\big(2-3\sin^2\vartheta\big)-
3\sin^2\vartheta\cos(2\phi)\Big\}
\times a^2\sin\theta\cos\theta\,\mathrm{d}\theta\,\mathrm{d}\phi\notag\\
&=\frac{4}{3\pi}\,\epsilon_0A^2a^5\int_0^{\pi/2}\mathrm{d}\theta\int_0^{2\pi}\mathrm{d}\phi
\,\sin^3\theta
\Big\{\big(2-3\sin^2\vartheta\big)\mathrm{P}_2(\cos\theta)+
\sin^2\vartheta\,\mathrm{P}_2^2(\cos\theta)\cos(2\phi)\Big\}\notag\\
&\quad\times\Big\{\big(2-3\sin^2\vartheta\big)-
3\sin^2\vartheta\cos(2\phi)\Big\}\notag\\
&=\frac{4}{3\pi}\,\epsilon_0A^2a^5
\int_0^{\pi/2}\mathrm{d}\theta
\sin^3\theta\Big\{\big(2-3\sin^2\vartheta\big)^2\,
\mathrm{P}_2(\cos\theta)\,2\pi-3\sin^4\vartheta\,\mathrm{P}_2^2(\cos\theta)\,\pi
\Big\}\notag\\
&=\frac{4}{3}\,\epsilon_0A^2a^5\int_0^{\pi/2}\mathrm{d}\theta\sin^3\theta
\Big\{\big(2-3\sin^2\vartheta\big)^2 (3\cos^2\theta-1)-
9\sin^4\vartheta\sin^2\theta\Big\}\notag\\
&=\frac{4}{3}\,\epsilon_0A^2a^5\int_0^{\pi/2}\mathrm{d}\theta\sin^3\theta
\Big\{\big(2-3\sin^2\vartheta\big)^2(2-3\sin^2\theta)-
9\sin^4\vartheta\sin^2\theta\Big\}\notag\\
&=\frac{4}{3}\,\epsilon_0A^2a^5\left\{
\big(2-3\sin^2\vartheta\big)^{\!2}
\left(2\,\frac{2}{3}-3\,\frac{4}{5}\,\frac{2}{3}\right)-
9\sin^4\vartheta\,\frac{4}{5}\,\frac{2}{3}\right\}\notag\\
&=\frac{4}{3}\,\epsilon_0A^2a^5\left\{
\big(2-3\sin^2\vartheta\big)^2\left(\frac{-4}{15}\right)-
\frac{24}{5}\,\sin^4\vartheta\right\}\notag\\
&=\frac{16}{45}\,\epsilon_0A^2a^5\left\{
-\big(2-3\sin^2\vartheta\big)^2-18\sin^4\vartheta\right\}\notag\\
&=\frac{16}{45}\,\epsilon_0A^2a^5\left(-4+12\sin^2\vartheta-27\sin^4\vartheta\right).
\label{e19}
\end{align}
The same result, apart from omission of the constant, can be obtained by integrating the torque with respect to~$\vartheta$.

A graph (Fig.~\ref{f4}) of the potential energy versus $\vartheta$ shows a stable minimum when $\vartheta=\pi/2$, meaning that the disc has its normal in the $xy$ plane ($z$-axis across a diameter). There is a maximum where the torque is zero, at $\vartheta=\vartheta_1=\arcsin(\sqrt{\smash[b]{2/9}})$. It has a metastable energy minimum at $\vartheta=0$, where the disc lies in the $xy$ plane. The stable minimum is $(49/4)\times$ deeper than the metastable minimum, so the metastable minimum is relatively quite shallow.
\begin{figure}
\centering\includegraphics{PotentialEnergy.eps}\caption[]{\label{f4}The potential energy of the disc, as a function of the angle $\vartheta$ that its normal makes to the $z$-axis. The vertical scale gives the value of the bracket $3(-4+12\sin^2\vartheta-27\sin^4\vartheta)$. This takes the value $-8$ at the maximum, so the minima are 4 and 49 units below the maximum.

There is a qualitative resemblance to the potential energy of a rod, reasoned out in \S\,\ref{s1.1}. However, the relative depths of the energy minima ($49:4$ instead of $4:1$) are different, and so is the angle~$\vartheta_1$.}
\end{figure}

Although we were careful to arrange that the disc is uncharged, for correctness, any overall charge (should an actual disc acquire one) would have no effect on the torque. (It would, however, have a great effect on a tendency of the disc to ``walk off'' from the origin.)

Even at its maximum, the potential energy of the disc is negative. This is because there is always a field $E_Y$, acting to push positive charge towards the $Y=0$ axis. So no orientation of the disc can make all components of the quadrupole moment equal to zero.


\raggedleft{
GAB, \qquad 16 March 2015}



\section*{References}
\noindent Abramowitz, M.\ and Stegun, A.\ (1965). \emph{Handbook of mathematical functions}. Dover, New York. Previously published by the National Bureau of Standards in 1964.

\noindent Landau, L.D., Lifshitz, E.M.\ and Pitaevski\u{\i}, L.P. (1993). \emph{Electrodynamics of continuous Media}, second edn. Course of Theoretical Physics, vol.\,8. Butterworth-Heinemann, Oxford. Formerly published by Pergamon, Oxford.




%\bibliography{Essays}
\end{document} 