\documentclass{book}
\usepackage[dvips]{graphicx}
\usepackage{amsmath,amsfonts,amssymb}
%\usepackage{epstopdf} %converting to PDF
\usepackage{color}      %% needed if your figures include grey as well as full black
\graphicspath{{Figures/}} 

\setlength{\textwidth}{160mm}
\setlength{\oddsidemargin}{0mm}
\setlength{\evensidemargin}{0mm}
\setlength{\textheight}{240mm}
\setlength{\topmargin}{0mm}
\setlength{\parskip}{0.1\baselineskip}
\setlength{\footnotesep}{4.5mm}

\newcommand{\harf}{\mbox{$\frac12$}}
\newcommand{\onethird}{\mbox{$\frac13$}}
\newcommand{\twothirds}{\mbox{$\frac23$}}
\newcommand{\fourthirds}{\mbox{$\frac43$}}
\newcommand{\onequarter}{\mbox{$\frac14$}}
\newcommand{\threequarters}{\mbox{$\frac34$}}
\newcommand{\onesixth}{\mbox{$\frac16$}}
\newcommand{\oneeigth}{\mbox{$\frac18$}}

\newcommand{\pda}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\pdb}[2]{\frac{\partial^2 #1}{\partial #2^2}}
\newcommand{\cpda}[2]{{\partial #1}/{\partial #2}}

\newcommand{\us}{u_{\mathrm s}}
\newcommand{\ep}{\epsilon_0}
\newcommand{\Uo}{\mathcal{U}_0}
\newcommand{\Vo}{V_0^{\prime\prime} }

\newcommand{\cth}{\cos\theta }
\newcommand{\sth}{\sin\theta }
\newcommand{\scth}{\sin\theta\cos\theta }

\newcommand{\Pz}{\mathrm{P}_0}
\newcommand{\PP}{\mathrm{P}_2}
\newcommand{\PPP}{\mathrm{P}_{2,1} }
\newcommand{\PPPP}{\mathrm{P}_{2,2} }

\newcommand{\PPc}{\mathrm{P}_2(\cos\theta)}
\newcommand{\PPPc}{\mathrm{P}_{2,1}(\cos\theta) }
\newcommand{\PPPPc}{\mathrm{P}_{2,2}(\cos\theta) }

\newcommand{\PPxi}{\mathrm{P}_2(\xi)}
\newcommand{\PPPxi}{\mathrm{P}_{2,1}(\xi) }
\newcommand{\PPPPxi}{\mathrm{P}_{2,2}(\xi) }

\newcommand{\Mz}{ M_0 (u) }
\newcommand{\MM}{ M_2 (u) }
\newcommand{\MMM}{ M_{2,1} (u)}
\newcommand{\MMMM}{ M_{2,2} (u)}

\newcommand{\Mzs}{ M_0 (u_{\mathrm s}) }
\newcommand{\MMs}{ M_2 (u_{\mathrm s}) }
\newcommand{\MMMs}{ M_{2,1} (u_{\mathrm s})}
\newcommand{\MMMMs}{ M_{2,2} (u_{\mathrm s})}

\newcommand{\dM}{\widetilde M_0^\prime (u_{\mathrm s}) }
\newcommand{\dMM}{\widetilde M_2^\prime (u_{\mathrm s}) }
\newcommand{\dMMM}{\widetilde M_{2,1}^\prime (u_{\mathrm s})}
\newcommand{\dMMMM}{\widetilde M_{2,2}^\prime (u_{\mathrm s})}

\newcommand{\wMz}{\widetilde M_0 (u) }
\newcommand{\wMM}{\widetilde M_2 (u) }
\newcommand{\wMMM}{\widetilde M_{2,1} (u)}
\newcommand{\wMMMM}{\widetilde M_{2,2} (u)}

\newcommand{\wMzs}{\widetilde M_0 (u_{\mathrm s}) }
\newcommand{\wMMs}{\widetilde M_2 (u_{\mathrm s}) }
\newcommand{\wMMMs}{\widetilde M_{2,1} (u_{\mathrm s})}
\newcommand{\wMMMMs}{\widetilde M_{2,2} (u_{\mathrm s})}


\def\thesection{\arabic{section}}
%\bibliographystyle{GABtoOUP}% file c:\TeXLocalFiles\bibtex\bst\Gab\GABtoOUP.bst

\begin{document}
\thispagestyle{empty}
\begin{center}
{\Large\bfseries  Induced dipole and quadrupole moments of a spheroid - short v.} \end{center}

This  calculation shows how the torque on a spheroidal conductor differs from that on an infinitely thin circular disc (of the same radius) for the two cases:
\begin{itemize}
\item the induced dipole moment in an uniform electric field, and 
\item the induced quadrupole moment in a quadrupole field. 
\end{itemize}
The first case is straightforward and the results have many applications whereas for the induced quadrupole moment we have carried out a calculation from first principles by using suitable solutions of Laplace's equation.\\  

For the particular case of a spheroid in an uniform electric field $E\hat{\mathbf e}_z$ the equivalence of all the radial directions in the reference frame of the object means that  we can chose coordinates such that there are just two components of the E-field (radial and axial); the polarizability depends only on the angle $\vartheta$ of its symmetry axis w.r.t.\ to the applied E-field in the lab frame. The component of the electric field  along the symmetry axis of the spheroid, $E\cos\vartheta$, gives rise to a energy of $\mathcal U= -\harf \alpha_{\parallel}E^2\cos^2\vartheta$ and similarly for the perpendicular component $E\sin\vartheta$. Hence potential energy arising from the induced dipole is 
\begin{equation}
\mathcal U_{\mathrm{iD}} = -\harf E^2 \left( \alpha_{\perp} \sin^2\vartheta + \alpha_{\parallel} \cos^2\vartheta \right)
\end{equation}
where the polarizibilities are such that $\alpha_{\perp} > \alpha_{\parallel}$ for an oblate spheroid.  The torque is given by 
\begin{align}
 \mathcal{T} 
  &= 
 -\frac{\partial \mathcal{U}_{\mathrm{iD}} }{\partial \vartheta} \\
   &= 
   E^2 \left( \alpha_{\perp}  - \alpha_{\parallel}\right) \sin\vartheta\cos\vartheta   
\end{align}
Thus $\mathcal{T}=0 $ at $\vartheta = 0$ which is a maximum of the potential energy. The torque is also zero at $\vartheta = \pi/2$; defining $\vartheta^\prime = \vartheta -\pi/2$ allows the restoring torque for small angles ($\vartheta^\prime \ll 1$) about this minimum to be written as 
\begin{align}
\mathcal{T}(\vartheta^\prime ) 
 & \simeq
   -\Gamma_{\mathrm{iD}}\vartheta^\prime \text{  where  }
   \\
  \Gamma_{\mathrm{iD}} 
  &=
    \left( \alpha_{\perp}  - \alpha_{\parallel}\right) E^2 . \label{GammaiD}
 \end{align}
The polarizibility of an infinitely thin disc of radius $R$ is 
\begin{align}
    \alpha_{\perp} 
     &=  \frac{ 16 }{3}\epsilon_0  R^3  
   \end{align}
Parallel to the symmetry axis $\alpha_{\parallel} =0$ since charges cannot be displaced in this direction if it is infinitely thin. (A sphere has an isotropic polarizability $\alpha_{\mathrm{sphere}} =  \frac{ 4\pi}{3}\epsilon_0  R^3 = (\pi/4)\alpha_{\mathrm{disc}}$.)  We rewrite \eqref{GammaiD} as
\begin{align}
  \Gamma_{\mathrm{iD}} 
  =
    \frac{ 16 }{3}\epsilon_0  R^3 E^2 h_{\mathrm{iD}} . 
 \end{align}
where $h_{\mathrm{iD}}$ is a function defined such that $h_{\mathrm{iD}} = 1$ for the disc and $h_{\mathrm{iD}}=0$ for a sphere; its dependence on the aspect ratio of the spheroid (radius over half-thickness), is derived below.\\

\begin{figure}[h]
\centering\includegraphics{QuadrupoleFieldLines.pdf}\caption{\label{QpoleFieldLines} (From Geoff) Field lines for the quadrupole potential $\varphi_{\text{applied}}=A(x^2+y^2-2z^2)$, shown in the $xz$ plane. The slanting line is the equipotential $\varphi=0$ and, being an equipotential, is perpendicular to all field lines. A conducting rod, lying along this line, has no induced charge movements. The line has $z=x/\sqrt{2}$; equivalently $\varTheta=\varTheta_1$ where $\mathrm{P}_2(\cos\varTheta_1)=\frac{1}{2}(3\cos^2\varTheta_1-1)=0$.}
\end{figure}

We consider a conducting spheroid in the quadrupole field shown in Fig.~\ref{QpoleFieldLines}) where the electrostatic potential is  
 \begin{equation}\label{qpolepotential}
\varphi_{\text{applied}} = \frac{\Vo}{2} \left( \frac{x^2+y^2}{2} -z^2 \right).
\end{equation}
The spheroid has an induced-quadrupole interaction energy of the order of   
\begin{align}\label{Uo}
  \Uo = \harf \epsilon_0 R^5(\onequarter V_0^{\prime\prime})^2 .  
\end{align}
Note that $R$ and $a$ will be different from now on.\\
An disc of radius $R$ experiences a restoring torque given by (in terms of the angle $\vartheta^\prime = \vartheta -\pi/2$)
\begin{align}
\mathcal{T}_{iQ}(\vartheta^\prime)  =
 - \Uo \frac{8}{15} \left( 9 \cos^2\vartheta^\prime  - 2 \right)\cos\vartheta^\prime\sin\vartheta^\prime  
  \end{align}
The deepest energy minimum occurs at $\vartheta^\prime=0$ (or equivalently $\vartheta = \pi/2$); the restoring torque for small angular displacements $\vert\vartheta^\prime\vert \ll 1$ can be written as $\mathcal{T}(\vartheta^\prime)  = - \Gamma_{\mathrm{iQ}}  \vartheta^\prime$ where the coefficient is 
 \begin{align}
   \Gamma_{\mathrm{iQ}}   
   =  
    \Uo \frac{56}{15}\, h_{\mathrm{iQ}} \label{hiQ}
  \end{align}
As above, we define a function with the value $h_{\mathrm{iQ}}= 1$ for an infinitely thin disc and $h_{\mathrm{iQ}}= 0$ for a sphere.\\
\begin{figure}[h]
\centering\includegraphics{hiDhiQpt6.pdf}\caption{\label{hiDhiQpt6} The functions $h_{\mathrm{iD}}$ and $h_{\mathrm{iQ}}$ for the induced dipole moment and induced quadrupole moments, that in each case give the ratio of the torque on a spheroid to that of an infinitely flat disc of the same radius. The plot shows that considering the system as a flat disc gives less than 10\% error over the range from $u=0$ (which is the limit of zero thickness where the aspect ratio is $\lambda = \infty$) up to $u=0.475$ which corresponds to $\lambda = 2.33$ in eqn \eqref{aspectratio}. This range covers all the objects considered in this paper.}
\end{figure}

\begin{figure}[h]
\centering\includegraphics{hiDhiQupto6.pdf}\caption{\label{hiDhiQupto6} A plot of the functions $h_{\mathrm{iD}}$ and $h_{\mathrm{iQ}}$ over a larger range of $u$ than in the previous Figure. After the maximum at small $u$ both functions fall to zero as spheroid becomes more spherical (the aspect ratio $\lambda \rightarrow 1$, from eqn \eqref{aspectratio} ). There must be reason why the shape of the curves are so similar but since this is only a 10\% effect for us, I'm not inclined to investigate further; we have the equations necessary to produce plots of the surface charge density for any alignment angle and $u$.}
\end{figure}

Figures~\ref{hiDhiQpt6} and \ref{hiDhiQupto6}  shows a plot of $h_{\mathrm{iD}}$ and $h_{\mathrm{iQ}}$ as a function of $u$. By construction these factors for the arising from induced moments are unity at $u=0$.  They both tend to zero as $u\rightarrow \infty$ (the limit of spherical symmetry). They also have similarly shaped curves with a maximum at small $u$ followed by a gradual falloff. The initial upward bump may arise because the induced charge has more area to spread out as the sharp edge of the disc turns into a rounded spheroid but as $u$ increases further that mechanism is out weighed as the surface becomes more spherical. 
Having summarised the results for a infinitely thin disc we now describe the derivation for the general case of finite thickness. The potential energy of both the induced quadrupole and dipole moments of a conducting spheroid in an external field can be calculated by the standard methods for finding solutions of Laplace's equation. The boundary conditions are that electrostatic potential is constant on the surface of the conductor and that fields arising from the induced charge tend to zero at large distances.\\ 

Oblate spheroidal coordinates can be defined by [LLP, Smythe]:   
\begin{align}\label{spheroidalcoords}
X&=a(1 + u^2)^{1/2}(1 - \xi^2)^{1/2}\cos\phi;\qquad &Y=& a(1 + u^2)^{1/2}(1 - \xi^2)^{1/2}\sin\phi;\qquad
&Z& =a u\xi .
\end{align}
The quantity $a$ has dimension of length. These coordinates are related through the equation
\begin{align}\label{spheroid}
  \frac{ X^2 + Y^2}{1 + u^2} + \frac{Z^2}{ u^2} = a^2 .
\end{align}
At large distances $u\gg1$ this tends towards the equation of a sphere and the radial coordinate $au \rightarrow r$, the usual spherical polar coordinate. Surfaces of constant $u$ are oblate spheroids of radius $R=a(1 + u^2)^{1/2}$, half-thickness $L=a u$ and hence aspect ratio given by 
\begin{equation}\label{aspectratio}
\lambda = \frac{(1 + u^2)}{u}^{1/2} \equiv \sqrt{1 + \frac{1}{u}} \ \ge 1
\end{equation}
This co-ordinate system allows separation of variables to find a solution of Laplace's equation as a product 
\begin{equation}\label{Laplace}
\varphi =M(u)\Xi(\xi)\varPhi(\phi).
\end{equation}
where $\varPhi(\phi)=\cos(m\phi)$ with $m$ an integer, and $\Xi(\xi) =\mathrm{P}_l^m(\xi)$ are associated Legendre polynomials of the first kind. (Similarity with the functions of the angular co-ordinates in the familiar case of spherical polars can be emphasised by the substitution $\xi = \cos\theta$, but note that this $\theta$ is different to external angle $\vartheta$.) The `radial' functions $M(u)$ are combinations of associated Legendre functions of the first and second kind which tend to zero as $u\rightarrow \infty$ [Smythe].\\

For an uniform electric field it is straightforward to determine the potential that is a solution to Laplace's for a conducting ellipsoid (and consider a spheroid as the special case in which 2 semi-axes have equal length); ellipsoidal co-ordinates are related to spheroidal co-ordinates (both oblate and prolate) [1]. The components of the uniform field along each of the three symmetry axes of the ellipsoid can be considered separately. From the solution for one direction, the equations for the other two directions can be written down (by permutation of co-ordinate labels). Superposition of the three solutions gives the full electrostatic potential. Thus the problem is reduced to solving Laplace's equation for an ellipsoid with an electric field along one of its axes of symmetry.%
\footnote{It is also straightforward to solve Laplace's equation for a dielectric ellipsoid with a relative permittivity $\epsilon_1$ in a medium with  $\epsilon_r =\epsilon_2$ (both media being linear, isotropic and homogeneous) in a uniform E-field by using the ellipsoidal co-ordinate system. A conductor corresponds to the limit $\epsilon_1\rightarrow \infty$. }\\

Polarizabilities are proportional to the volume of the object divided by a depolarising factor $n$: \ $\alpha_\perp =  \epsilon_0 V/ n_\perp$ and similarly for $\alpha_\parallel$; a spheroid of radius $R$ and half-thickness $L=R/\lambda$ has volume $V= \fourthirds \pi R^3 /\lambda $, thus \begin{align}
   \left( \alpha_{\perp}  - \alpha_{\parallel}\right) =  \fourthirds \pi \epsilon_0  R^3 \left( \frac{1}{\lambda n_{\perp} }  - \frac{1}{\lambda n_{\parallel} } \right). 
 \end{align}
The  depolarising factors obey the condition $2n_{\perp} + n_{\parallel} =1$ (so that $n_{\parallel} = n_{\perp} = 1/3$ for a sphere). Thus
\begin{align}\label{perptoparallel}
 n_{\perp}  
  = \harf ( 1 - n_{\parallel})
 \end{align}
For an electric field along the axis of the spheroid [1] 
\begin{align}
 n_{\parallel}
  &= 
 \left(1 +u^2\right) \left\{1-u M_0(u)\right\}.
\end{align}
The function $M_0$ is given by\footnote{A combination of Legendre functions of the first and second kind (for $l=0$).}  
\begin{equation}\label{M0}
M_0(u) = \pi/2 - \arctan(u) \equiv \arctan(1/u).  
\end{equation}
This function has the value $M_0(0) =\pi/2$, and $M_0(u) \rightarrow 0$ as $u\rightarrow \infty$. The corresponding values of the aspect ratio defined in \eqref{aspectratio} are $\lambda = \infty$ (limitingly thin disc) and $\lambda \rightarrow 1$ as $u \rightarrow \infty$; ($\lambda =1$ is a sphere). Using \eqref{perptoparallel} we find the expressions (valid for $u\ge 0$) and their limits, 
 \begin{align}
   \frac{1}{\lambda n_{\perp} }
   & = \frac{2u}{  \left(1 +u^2\right)^{1/2}\left\{ \left(1 +u^2\right) u\left( \frac{\pi}{2} - \arctan(u) \right) - u^2 \right\}  }  &\rightarrow \frac{4}{\pi}
   \qquad \text{as } u\rightarrow 0 \label{limit1}
    \\
   \frac{1}{\lambda n_{\parallel} } 
    & =
     \frac{u}{  \left(1 +u^2\right)^{3/2} \left\{ 1 - u\left( \frac{\pi}{2} - \arctan(u) \right) \right\}  }  &\rightarrow  O(u)    \qquad \text{as } u\rightarrow 0 \label{limit2}
  \end{align}

The function $h_{\mathrm{iD}}(u)$ in \eqref{GammaiD}, defined such that $h_{\mathrm{iD}}(0) = 1$, is given by  
\begin{align}
  h_{\mathrm{iD}}  =  \frac{ \pi }{4} \left(  \frac{1}{\lambda n_{\perp} }  - \frac{1}{\lambda n_{\parallel} } \right)
 \end{align}
Substituting in the quantities from \eqref{limit1} and \eqref{limit2} gives an expression (not written out here) which is plotted as a function of $u$  in Fig.~\ref{hiDhiQpt6} and \ref{hiDhiQupto6}. Since $h_{\mathrm{iD}}(0.61) = 0.9$ and $u=0.61$ corresponds to $\lambda = 1.9$ in \eqref{aspectratio}, we find that $\Gamma_{\mathrm{iD} }$ is within 10\% of the value for an infinitely thin disc over the range from $\lambda = \infty$ to $1.9$ (which covers all the objects we consider in this paper).\\ 
 
A conducting spheroid in a quadrupole field with electrostatic potential given by \eqref{qpolepotential} is more complicated than that of the induced dipole since the trick of considering components of the E-field along the axes of the object independently cannot be used (and for quadrupole moments we consider only the spheroid, not the more general case of an ellipsoid). By following the methods described in [1,2] we calculate the potential energy, arising from the quadrupole moment interacting with the quadrupole field that induces it, to be 
 \begin{equation}\label{UiQ}
 \mathcal{U}_{iQ} = \Uo \frac{2}{45} \left(-4 b_0+12b_2\sin^2\vartheta-27b_4\sin^4\vartheta\right).
\end{equation}  
where $\Uo$ is defined in \eqref{Uo}; the functions $b_0, b_2, b_4$ are defined such that $b_n\rightarrow 1$ as $u\rightarrow 0$ (for $n =0,1,2$); 
\begin{align}
  b_0  
 &=
   \frac{\pi}{8} D_{20}   \label{b0}
  \\
%  & \simeq
%   (1 + 0.19 u \ldots )    \qquad &\text{for small } u \nonumber
%   \\
     b_2    
   &=
     \frac{\pi}{8}  \left(  D_{20} - 9 D_{21} \right) \label{b2}
    \\
      b_4  
    &= 
     \frac{\pi}{24} \left(  D_{20} - 12 D_{21} + 3 D_{22}\right)   \label{b4}
\end{align}
The functions of $u$ associated with each of the Legendre functions $\mathrm{P}_l^m(\xi)$ for $l=2$ and $m=0,1,2$ are
\begin{align}
D_{20} &=  \frac{2(1+3u^2)}{\left(1+u^2\right)^{5/2} } \left( \frac{ 2}{\Mz(1+3u^2)-3u} - 3u \left(1+u^2 \right) \right)  &\rightarrow \frac{8}{\pi} \qquad &\text{as } u\rightarrow 0 \label{D20} \\
D_{21} &= \frac{ - u}{ \left(1+u^2 \right)^{3/2} } \left( \frac{2 } {3 \Mz \left(u +u^3 \right) -2 -3 u^2 } + 1+2u^2 \right) &\rightarrow \frac{3\pi}{4}u^2 \qquad &\text{as } u \rightarrow 0\label{D21} \\
D_{22} &=   \frac{2}{ \left(1+u^2\right)^{1/2} } \left( \frac{4}{3 \Mz \left(1+u^2\right)^2 -5u - 3 u^3} -u\right) &\rightarrow \frac{16}{3\pi} \qquad &\text{as } u\rightarrow 0 \label{D22} 
\end{align}
Note that $D_{21}$, derived from the function $M(u)$ that multiplies $\mathrm{P}_1^1(\xi)\cos\phi$ in \eqref{Laplace}, only has a second-order effect for $u \ll 1$.  The torque is given by 
\begin{align}
  \mathcal{T} 
 &= 
   -\frac{\partial \mathcal{U}_{\text{iQ}}}{\partial \vartheta}
  \\
  &= 
    - \Uo \frac{8}{15}\,  \left( 2b_2 - 9 b_4\sin^2\vartheta\right)\sin\vartheta\cos\vartheta    
\end{align}
Hence $\mathcal{T}=0 $ at $\vartheta =0$ and $\pi/2$ where there are minima of the potential energy $\mathcal{U}_{\text{iQ}}$. The torque is also zero at the intermediate angle $\vartheta_{\mathrm{max}} = \sin^{-1}( \sqrt{2b_2/ 9 b_4})$, where there is a maximum of $\mathcal{U}_{\text{iQ}}$ (if $u < 0.6333$, above this value the maximum is at $\vartheta_{\mathrm{max}} =0$, i.e.\ the secondary minimum does not exist). The ratio of potential energies at the  minima (when $u=0$ and hence $b_0 =b_2 =b_4 =1$ in \eqref{UiQ})  is  
\begin{equation}
 \frac{\left[\,\mathcal{U}\, \right]_{\vartheta =\pi/2} }{ \left[\, \mathcal{U} \, \right]_{\vartheta =0 } }  = \frac{19}{4} = 3.75
\end{equation}
This ratio is 4 for an infinitely thin conducting rod (limiting case of a prolate spheroid) because the induced quadrupole energy is proportional to $(V^{\prime\prime})^2$ and the second derivatives in Laplace's equation are related by $2V_{xx}=2V_{yy}=V_{zz}=V_0^{\prime\prime}$ for the potential in eqn \eqref{qpolepotential}. To quantify the strength of the induced quadrupole interaction we consider the  torque close to deepest minimium at $\vartheta =\pi/2$, and define $\vartheta^\prime = \vartheta -\pi/2$ so that the restoring torque for small angles, $\vert\vartheta^\prime\vert \ll 1$, can be written in the form 
\begin{align}
\mathcal{T}(\vartheta^\prime)  =
  \Uo \frac{8}{15} \left( 2b_2 - 9 b_4\cos^2\vartheta^\prime\right)\cos\vartheta^\prime\sin\vartheta^\prime  \simeq  - \Gamma_{\perp} \vartheta^\prime  
  \end{align}
where the coefficient  
 \begin{align}
   \Gamma_{\perp}  
   =  
    \Uo \frac{56}{15}\, h_{\text{iQ}} \nonumber
 \end{align}
The function, defined such that $h_{\text{iQ}}(0) = 1$, is 
\begin{align}
   h_{\text{iQ}}\left( u \right) 
   &=  \left( \frac{ 9 b_4- 2b_2}{7} \right) \nonumber
   \\
   \
   &= 
   \frac{\pi}{56} \left(  D_{20} - 18 D_{21} + 9 D_{22}\right) 
  \end{align}
Substituting in the quantities from \eqref{D20}, \eqref{D21}, and  \eqref{D22} gives a large algebraic expression (not written out here) which is plotted as a function of $u$ in Figures~\ref{hiDhiQpt6} and \ref{hiDhiQupto6}. The slow variation of $h_{\text{iQ}}\left( u \right)$ with $u$ means the value of  $\Gamma_{\mathrm{iQ} }$ for an infinitely thin disc gives a good approximation in all the cases that we consider, e.g.\ $h_{\text{iQ}}(0.475) = 0.9$ and $u=0.475$ corresponds to $\lambda = 2.33$, in \eqref{aspectratio}, so there is less than 10\% error over the range from $\lambda = \infty$ to $2.33$. \\
 
For most purposes we can use the value for an infinitely thin disc. Table~\ref{TableFactors} gives the correction factors for various aspect ratios $\lambda = R/L \ge 1$ and corresponding values of $u$.

\begin{table}[htdp]
\caption{The correction factors for some values of the $\lambda$ and $u$. }
\begin{center}
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
 $\lambda$ & $\infty$ & 3.5 & 1.9 & 1.4 & 1.05 & 1.04 \\
\hline
 $ u$  & 0 & 0.3 & 0.6 & 1 & 3  & 6\\
 \hline 
  $ h_{\text{iD} } $ & 1 & 1.04 & 0.9 & 0.65 & 0.14 & 0.04 \\
 \hline 
  $ h_{\text{iQ} }$ & 1 & 1.00 & 0.81 & 0.52 & 0.09 & 0.02 \\
 \hline 
\end{tabular}
\end{center}
\label{TableFactors}
\end{table}%

It is appropriate to use $u$ since we are interested in the limit $u\rightarrow 0$. For small deformations from the nearly spherical case one should use $1/u$, with $M_0=\arctan(1/u)$, or $1/u = \sqrt{\lambda^2 -1}$ (for $\lambda \ge 1$).\\

Added. In the limit $u\rightarrow \infty$ both $b_2\rightarrow 0$ and $b_4\rightarrow 0$ (no angular dependence). These imply that $\left[ \frac{1}{9} D_{20}= D_{21} = D_{22}\right]_{u=\infty}$ which is satisfied by  $\left[ (D_{20}, D_{21}, D_{22})\right]_{u=\infty} = (-18, -2, -2)$.\\

One day I should do all this again for a linear qpole, or even one with arbitrary coefficients ? 




\section*{References}

\noindent Landau, L.D., Lifshitz, E.M.\ and Pitaevski\u{\i}, L.P. (1993). \emph{Electrodynamics of continuous Media}, second edn. Course of Theoretical Physics, vol.\,8. Butterworth-Heinemann, Oxford. Formerly published by Pergamon, Oxford.

\noindent Smythe, W.R.,  (1950). \emph{Static and dynamic electricity}, . 

%\bibliography{Essays}
\end{document} 